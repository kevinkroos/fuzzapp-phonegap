
var deviceId;
var userid;
var userpass;

var errorHappened = false;

$( document ).ajaxStop(function() {
    $('body').css('display', 'block');
});

$( document ).ajaxError(function( event, request, settings ) {

    if (errorHappened)
        return;

    errorHappened = true;

    navigator.notification.confirm('Can\'t connect to the internet', retryConnection, 'Error', 'Retry');
});

function retryConnection(button) {

    if(button == 1) {
        startUp();
    }
}

function startUp() {

    $(document).ready(function() {

        errorHappened = false;

        getDeviceId();

        userid = localStorage.getItem("username");
        userpass = localStorage.getItem("password");
        deviceId = localStorage.getItem("deviceId");

        $.ajax({
            url:"https://my.fuzztec.com/fc_service/getinfo/" + localStorage.getItem("deviceId") +"/" + localStorage.getItem("username") +"/"+ localStorage.getItem("password"),
            success:function(data) {

                var d = JSON.parse(data);

                if(d.workaction == "on") {
                    changesColorsGreen();
                } else {
                    changesColorsRed();
                }
            },
            error:function() {
                //alert("ErrorCOLORCHECK");
            }
        });

        setData();
    });



}

function getDeviceId() {

    $.ajax({
        url:"https://my.fuzztec.com/fc_service/getdeviceid/"+ localStorage.getItem("username") +"/"+ localStorage.getItem("password"),
        success:function(data) {

            var d = JSON.parse(data);

            localStorage.setItem("deviceId", d.deviceid);

            $('#deviceid').text(localStorage.getItem("deviceId"));
        },
        error:function(data) {
            //alert(data.Message);
        }
    });
}

function timeInButton() {

    $.ajax({
        url:"https://my.fuzztec.com/fc_service/checktime/"+ localStorage.getItem("deviceId") +"/" + localStorage.getItem("username") + "/"+ localStorage.getItem("password") +"/on",
        global: false,
        success:function(data) {
            changesColorsGreen();
            setData();

            navigator.notification.alert('You are clocked in', null, 'Done', 'Ok');

        },
        error:function() {
            navigator.notification.alert('Can\'t connect to the internet', null, 'Error', 'OK');
            return;
        }
    });
}

function timeOutButton() {

    $.ajax({
        url:"https://my.fuzztec.com/fc_service/checktime/"+ localStorage.getItem("deviceId") +"/" + localStorage.getItem("username") + "/"+ localStorage.getItem("password") + "/off",
        global: false,
        success:function(data) {
            changesColorsRed();
            navigator.notification.alert('You are clocked out', null, 'Done', 'Ok');
        },
        error:function() {
            navigator.notification.alert('Can\'t connect to the internet', null, 'Error', 'OK');
            return;
        }
    });

    setData();

}

function changesColorsRed() {
    $("#dataTable").attr('class', 'red');
    $("#header").attr('class', 'red');
    $('#status').text("Time Out");
    $('#buttons').removeClass();
    $('#buttons').addClass('red');
    $('#bottomBar').removeClass();
    $('#bottomBar').addClass('red');

    //Checkout actief
    $('#timeout').addClass('active');
    $('#timeout img').first().attr('src', 'img/stopred-active.png');

    //Checkin button inactief
    $('#timein').removeClass();
    $('#timein img').first().attr('src', 'img/play.png');


}

function changesColorsGreen() {
    $("#dataTable").attr('class', 'green');
    $("#header").attr('class', 'green');
    $('#status').text("Time IN");
    $('#buttons').removeClass();
    $('#buttons').addClass('green');
    $('#bottomBar').removeClass();
    $('#bottomBar').addClass('green');

    //Checkout actief
    $('#timeout').removeClass();
    $('#timeout img').first().attr('src', 'img/stop.png');

    //Checkin button inactief
    $('#timein').addClass('active');
    $('#timein img').first().attr('src', 'img/playgreen-active.png');

}

function setData() {

    $.ajax({
        url:"https://my.fuzztec.com/fc_service/getinfo/" + localStorage.getItem("deviceId") + "/" + localStorage.getItem("username") +"/"+ localStorage.getItem("password"),
        success:function(data){

            var d = JSON.parse(data);

            $('#timeToday').text(d.worktime);
            $('#timeWeek').text(d.weektime);
        },
        error:function(){
            //alert("Error");
        }
    });
}

function login() {

    var username = $('#username').val();
    var password = $('#password').val();

    $.ajax({
        url:"https://my.fuzztec.com/fc_service/login/"+ username +"/"+ password,
        success:function(data){

            var d = JSON.parse(data);

            if(d.result) {
                localStorage.setItem("username", username);
                localStorage.setItem("password", password);

                window.location="index.html";

            } else {
                navigator.notification.alert('Incorrect login/password', null, 'Error', 'Ok');
            }

        },
        error:function(data){
            navigator.notification.alert('Can\'t connect to the internet', null, 'Error', 'Ok');
        }
    });
}

function checkLogin() {

    if(window.localStorage.getItem("username") != null && window.localStorage.getItem("password") != null) {

        window.location="index.html";
    }
}

function logout() {

    window.localStorage.removeItem("username");
    window.localStorage.removeItem("password");
    window.localStorage.removeItem("deviceId");

    navigator.notification.alert('You are logged out', null, 'Done', 'Ok');

    window.location="login.html";
}
